# README #

Descripcion

### Tecnologias usadas ###

* Maven v. 3.3 gestion de dependencias
* MySQL v. 5.7.12-log
* Framewoks: Spring, dropwizard, hibernate

### Instalacion y uso ###

* Instalar MySQL en un servidor de bases de datos
* Ejecutar el script Movies/Database.sql para crear la base de datos y el usuario
* Configurar en el archivo Movies/dataaccess/src/main/resources/hibernate.cfg.xml lo relacionado a la conexion con la base de datos (IP y puerto del servidor, usuario, contraseña, nombre de base de datos)
* Para compilar la biblioteca **dataaccess** ejecutar por consola de comandos:
* * cd Movies/dataaccess
* * mvn clean install
* * Si la compilacion es satisfactoria, colocar el archivo Movies/dataaccess/target/persistence-1.0-jar-with-dependencies.jar en la carpeta Movies/api/lib
* Para compilar la API Rest ejecutar por consola de comandos:
* * cd Movies/api
* * mvn clean install (asegurese de compilar previamente el proyecto dataaccess)
* * Si la compilacion es satisfactoria, proceder a ejecutar: *java -jar target/api-1.0.jar server MoviesConfiguration.yml* . Este comando expone un API Restful que sirve documentos el CRUD de peliculas en la URL http://localhost:9000/movies

### Pendientes ###

* Pruebas de integracion API
* Capa de presentacion
* Funcionalidad para generacion de reporte