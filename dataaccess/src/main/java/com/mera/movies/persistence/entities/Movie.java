
/**
 * 
 */
package com.mera.movies.persistence.entities;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author DavidCamilo
 * <p>This POJO class represents Movies (Peliculas) tables on the database</p>
 */
@Entity
@Table(name = "Pelicula" )
public class Movie implements Serializable {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = -4993661170731108041L;
	
	/**
	 * The identifier of the movie into the database
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IdPelicula")
	private Long movieId;
	
	/**
	 * Another reference to identify a movie
	 */
	@NotEmpty
	@Column(name="CodigoPelicula")
	private String movieCode;
	
	/**
	 * The name of the movie
	 */
	@NotEmpty
	@Column(name="NombrePelicula")
	private String movieName;
	
	/**
	 * A text which describes the movie
	 */
	@NotEmpty
	@Column(name="DescripcionPelicula")
	private String movieDescription;
	
	/**
	 * The images associated to the movie
	 */
	@OneToMany(fetch=FetchType.LAZY, cascade=CascadeType.ALL)
	private Set<Image> images;
	
	/**
	 * @return the movieId
	 */
	public Long getMovieId() {
		return movieId;
	}

	/**
	 * @return the movieCode
	 */
	public String getMovieCode() {
		return movieCode;
	}

	/**
	 * @return the movieName
	 */
	public String getMovieName() {
		return movieName;
	}

	/**
	 * @return the movieDescription
	 */
	public String getMovieDescription() {
		return movieDescription;
	}
	
	/**
	 * @return the images
	 */
	public Set<Image> getImages() {
		return images;
	}

	/**
	 * @param movieId the movieId to set
	 */
	public void setMovieId(@NotNull Long movieId) {
		this.movieId = movieId;
	}

	/**
	 * @param movieCode the movieCode to set
	 */
	public void setMovieCode(@NotEmpty String movieCode) {
		this.movieCode = movieCode;
	}

	/**
	 * @param movieName the movieName to set
	 */
	public void setMovieName(@NotEmpty String movieName) {
		this.movieName = movieName;
	}

	/**
	 * @param movieDescription the movieDescription to set
	 */
	public void setMovieDescription(@NotEmpty String movieDescription) {
		this.movieDescription = movieDescription;
	}

	/**
	 * @param images the images to set
	 */
	public void setImages(Set<Image> images) {
		this.images = images;
	}

}
