/**
 * 
 */
package com.mera.movies.persistence.dao;

import java.util.List;

/**
 * @author DavidCamilo
 * <p>This interfaces represents the operations to be perfomed
 * on the database.</p>
 */
public interface IDAO<T> {
	
	/**
	 * This method allows to add new data in the database
	 * @param newItem The item to be saved into the database
	 */
	public void add(T newItem);
	
	/**
	 * This method allows to update data in the database
	 * @param updatedItem The updated data for a given item
	 */
	public void update(T updatedItem);
	
	/**
	 * This method allows to get all Items in a specific table
	 * @return All items saved
	 */
	public List<T> getAll();
	
	/**
	 * Return the required item which satisfies a given identifier
	 * @param id The item identifier
	 * @return The item which satisfies the criterion 
	 */
	public T getItemById(Long id);
	
	/**
	 * This method removes data from the database
	 * @param id The identifier of the item to be deleted
	 */
	public void delete(Long id);

}
