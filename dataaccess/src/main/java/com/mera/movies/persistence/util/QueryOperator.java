/**
 * 
 */
package com.mera.movies.persistence.util;

/**
 * @author DavidCamilo
 * An enumerations which contains common performed operations against database
 */
public enum QueryOperator {

	OPERADOR_AND,
	OPERADOR_OR,
	MAYOR_QUE,
	MAYOR_IGUAL_QUE,
	MENOR_QUE,
	MENOR_IGUAL_QUE,
	IGUAL,
	DIFERENTE,
	CONTIENE,
	ORDENAR_POR_ASCENDENTEMENTE,
	ORDENAR_POR_DESCENDENTEMENTE,
	ES_NULO,
	NO_ES_NULO,
	ENTRE_RANGO;
}
