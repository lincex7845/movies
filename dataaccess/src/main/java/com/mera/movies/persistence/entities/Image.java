/**
 * 
 */
package com.mera.movies.persistence.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author DavidCamilo
 * <p>This POJO class represents Images (Imagenes) table on the database</p>
 */
@Entity
@Table(name = "Imagen" )
public class Image implements Serializable {

	/**
	 * Generated serial version UID
	 */
	private static final long serialVersionUID = 1273079310434465010L;

	/**
	 * The identifier of the image into the database
	 */
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="IdImagen")
	private Long imageId;
	
	/**
	 * The file name to retrieve the image
	 */
	@NotEmpty
	@Column(name="NombreImagen")
	private String fileName;
	
	/**
	 * The raw data which represent the image
	 */
	@NotNull
	private byte[] bytes;

	/**
	 * @return the imageId
	 */
	public Long getImageId() {
		return imageId;
	}

	/**
	 * @return the fileName
	 */
	public String getFileName() {
		return fileName;
	}

	/**
	 * @return the bytes
	 */
	public byte[] getBytes() {
		return bytes;
	}

	/**
	 * @param imageId the imageId to set
	 */
	public void setImageId(@NotNull Long imageId) {
		this.imageId = imageId;
	}

	/**
	 * @param fileName the fileName to set
	 */
	public void setFileName(@NotEmpty String fileName) {
		this.fileName = fileName;
	}

	/**
	 * @param bytes the bytes to set
	 */
	public void setBytes(@NotNull byte[] bytes) {
		this.bytes = bytes;
	}
}
