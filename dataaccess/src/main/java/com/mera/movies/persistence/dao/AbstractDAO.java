/**
 * 
 */
package com.mera.movies.persistence.dao;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.PersistenceException;
import javax.validation.constraints.NotNull;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.resource.transaction.spi.TransactionStatus;
import org.hibernate.stat.Statistics;
import org.jboss.logging.Logger;

import com.mera.movies.persistence.util.DataContextUtil;
import com.mera.movies.persistence.util.QueryCriteria;
import com.mera.movies.persistence.util.QueryCriteriaConverter;
import com.mera.movies.persistence.util.QueryOperator;

/**
 * @author DavidCamilo
 *
 */
public abstract class AbstractDAO<T> implements IDAO<T> {

	/***
	 * The logger instance
	 */
	protected final Logger LOGGER = Logger.getLogger(AbstractDAO.class);

	/**
	 * Ensures that every client gets his correct session
	 */
	private static final ThreadLocal<Session> SESSIONS = new ThreadLocal<>();

	/***
	 * Statistics provided by session object
	 */
	private Statistics stats;
	/**
	 * 
	 */
	private final Class<T> typeParameterClass;
	
	/**
	 * Constructor
	 * @param typeParameterClass The type which is resolved in runtime
	 */
	protected AbstractDAO(Class<T> typeParameterClass){
		this.typeParameterClass = typeParameterClass;
		stats = getStats();
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.mera.movies.persistence.dao.IDAO#getItemById(java.lang.Long)
	 */
	public T findItemById(@NotNull Long itemId){
		T item = (T)getSession().get(typeParameterClass, itemId);
		closeSession();
		return item;
	}
	
	/*
	 * (non-Javadoc)
	 * @see com.mera.movies.persistence.dao.IDAO#getAll()
	 */
	public List<T> getAllItems(){
		return filterByCriteria();
	}

	/**
	 * Gets the current hibernate session. Also takes care that there's always
	 * an open hibernate transaction when needed. *
	 * 
	 * @return Current hibernate session
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 * @throws HibernateException
	 *             Indicates a problem opening the session
	 */
	protected Session getSession() {
		Session result = SESSIONS.get();
		if (result == null) {
			result = DataContextUtil.getSessionFactory().openSession();
			SESSIONS.set(result);
			result.beginTransaction();
			LOGGER.info("Transaction initialized");
		}
		return result;
	}
	
	/**
	 * Stores or updates data into the database
	 * @param item The item to be saver or updated
	 */
	protected void saveOrUpdateItem(@NotNull T item){
		getSession().saveOrUpdate(item);
		closeSession();
	}
	
	/**
	 * Removes an item from the database
	 * @param item The item to be deleted
	 */
	protected void delete(@NotNull T item){
		getSession().delete(item);
		closeSession();
	}
	
	@SuppressWarnings("unchecked")
	protected List<T> filterByCriteria(QueryCriteria... queryCriteria){
		getSession();
		Criteria criteria = getSession().createCriteria(typeParameterClass);
		for(QueryCriteria criterion : queryCriteria){
			QueryOperator operator = criterion.getOperator();
			if(operator.equals(QueryOperator.ORDENAR_POR_ASCENDENTEMENTE) ||
					operator.equals(QueryOperator.ORDENAR_POR_DESCENDENTEMENTE)){
				criteria.addOrder(QueryCriteriaConverter.convertToOrder(criterion));
			}
			else{
				criteria.add(QueryCriteriaConverter.convertToCriterion(criterion));
			}
		}
		Set<T> uniqueItems = new HashSet<>(criteria.list());
		closeSession();
		return new ArrayList<>(uniqueItems);
	}
	
	/**
	 * Closes the current hibernate session, if there is one.
	 */
	protected void closeSession() {
		Session sess = SESSIONS.get();
		if (sess == null || !sess.isOpen()) {
			return;
		}
		SESSIONS.remove();
		try {
			if (sess.getTransaction().getStatus().equals(TransactionStatus.ACTIVE)) {
				sess.getTransaction().commit();
				LOGGER.info("Transaction Committed");
			}
		} catch (org.hibernate.exception.ConstraintViolationException e) {
			LOGGER.error("Constraint Violation", e);;
		} catch (HibernateException e) {
			sess.getTransaction().rollback();
			LOGGER.error("Rollback Transaction", e);
		} finally {
			try {
				sess.close();
				LOGGER.info(String.format(
						"Opened Session: %d. Closed Session: %d.",
						stats.getSessionOpenCount(),
						stats.getSessionCloseCount()));
			} catch (HibernateException th) {
				LOGGER.error("Close Session failed. ", th);
			}
		}
	}

	/***
	 * This method initializes statistics for SessionFactoryUtil object
	 * 
	 * @return The Statistics Object
	 * @throws PersistenceException
	 *             Usually indicates an invalid configuration or invalid mapping
	 *             information
	 */
	private Statistics getStats() {
		Statistics statics = DataContextUtil.getSessionFactory().getStatistics();
		statics.setStatisticsEnabled(true);
		return statics;
	}
}
