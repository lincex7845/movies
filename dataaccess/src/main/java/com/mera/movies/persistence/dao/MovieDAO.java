/**
 * 
 */
package com.mera.movies.persistence.dao;

import java.util.List;
import java.util.NoSuchElementException;

import org.hibernate.Hibernate;
import org.springframework.stereotype.Repository;

import com.mera.movies.persistence.entities.Image;
import com.mera.movies.persistence.entities.Movie;
import com.mera.movies.persistence.util.QueryCriteria;

/**
 * @author DavidCamilo
 * <p>The data access object for Movies table</p>
 */
@Repository
public class MovieDAO extends AbstractDAO<Movie> {

	public MovieDAO() {
		super(Movie.class);
	}

	/*
	 * (non-Javadoc)
	 * @see com.mera.movies.persistence.dao.IDAO#add(java.lang.Object)
	 */
	@Override
	public void add(Movie newItem) {
		saveOrUpdateItem(newItem);		
	}

	/*
	 * (non-Javadoc)
	 * @see com.mera.movies.persistence.dao.IDAO#update(java.lang.Object)
	 */
	@Override
	public void update(Movie updatedItem) {
		saveOrUpdateItem(updatedItem);
		
	}

	/*
	 * (non-Javadoc)
	 * @see com.mera.movies.persistence.dao.IDAO#getAll()
	 */
	@Override
	public List<Movie> getAll() {
		return getAllItems();
	}

	/*
	 * (non-Javadoc)
	 * @see com.mera.movies.persistence.dao.IDAO#getItemById(java.lang.Long)
	 */
	@Override
	public Movie getItemById(Long id) {
		Movie movie = findItemById(id);
		if(movie != null){
			return movie;
		}
		throw new NoSuchElementException("There is no exists a movie with the identifier " + id);
	}

	/*
	 * (non-Javadoc)
	 * @see com.mera.movies.persistence.dao.IDAO#delete(java.lang.Long)
	 */
	@Override
	public void delete(Long id) {
		delete(getItemById(id));
	}

	/**
	 * Performs a lazing loading of images related to a given movie
	 * @param movie The given movie to load its images
	 * @return The movie with the images associated to it
	 */
	public Movie getMovieWithImages(Movie movie){
		getSession().update(movie);
		Hibernate.initialize(movie.getImages());
		closeSession();
		return movie;
	}
	
	/**
	 * Adds an image to the movie
	 * @param movie The movie will be modified
	 * @param newImage The image to be saved
	 **/
	public void addImage(Movie movie, Image newImage){
		movie.getImages().add(newImage);
		saveOrUpdateItem(movie);
	}
	
	public List<Movie> getMoviesByCriteria(QueryCriteria... queryCriteria){
		return filterByCriteria(queryCriteria);
	}
}
