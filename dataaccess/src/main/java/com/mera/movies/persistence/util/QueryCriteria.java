/**
 * 
 */
package com.mera.movies.persistence.util;

/**
 * @author DavidCamilo
 * <p>A POJO class to help perform queries and restrictions on the database</p>
 */
public class QueryCriteria {

	private String attribute;
	private QueryOperator operator;
	private Object value;
	
	public QueryCriteria(){
		
	}
	
	public QueryCriteria(String attribute, QueryOperator operator, Object value)
	{
		this.attribute = attribute;
		this.operator = operator;
		this.value = value;
	}

	/**
	 * @return the attribute
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * @return the operator
	 */
	public QueryOperator getOperator() {
		return operator;
	}

	/**
	 * @return the value
	 */
	public Object getValue() {
		return value;
	}

	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}

	/**
	 * @param operator the operator to set
	 */
	public void setOperator(QueryOperator operator) {
		this.operator = operator;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(Object value) {
		this.value = value;
	}
}