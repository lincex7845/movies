/**
 * 
 */
package com.mera.movies.persistence.util;

import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;

/**
 * @author DavidCamilo
 * <p>A helper to transform {@link QueryCriteria} into
 * {@link org.hibernate.criterion} objects in order to perform
 * queries by using Hibernate</p>
 */
public final class QueryCriteriaConverter {
	
	/**
	 * Constructor by default
	 */
	private QueryCriteriaConverter(){
		
	}

	/**
	 * This method turns {@link QueryCriteria} into {@link Criteria}
	 * @param queryCriteria
	 * @return
	 */
	public static Criterion convertToCriterion(QueryCriteria queryCriteria)
	{
		Criterion criterion = null;		
		QueryOperator operador = queryCriteria.getOperator();
		switch (operador)
		{
			case IGUAL:
				criterion = Restrictions.eq(queryCriteria.getAttribute(), queryCriteria.getValue());
				break;
			case DIFERENTE:
				criterion = Restrictions.ne(queryCriteria.getAttribute(), queryCriteria.getValue());
				break;
			case MAYOR_QUE:				
				criterion = Restrictions.gt(queryCriteria.getAttribute(), queryCriteria.getValue());
				break;
			case MAYOR_IGUAL_QUE:
				criterion = Restrictions.ge(queryCriteria.getAttribute(), queryCriteria.getValue());
				break;
			case MENOR_QUE:				
				criterion = Restrictions.lt(queryCriteria.getAttribute(), queryCriteria.getValue());
				break;
			case MENOR_IGUAL_QUE:
				criterion = Restrictions.le(queryCriteria.getAttribute(), queryCriteria.getValue());
				break;
			case CONTIENE:
				criterion = Restrictions.ilike(queryCriteria.getAttribute(), queryCriteria.getValue());
				break;
			case ES_NULO:
				criterion = Restrictions.isNull(queryCriteria.getAttribute());
				break;
			case NO_ES_NULO:
				criterion = Restrictions.isNotNull(queryCriteria.getAttribute());
				break;
		default:
			break;			
		}
		return criterion;
	}
	
	/**
	 * This method transforms {@link QueryCriteria} objects into {@link Order} 
	 * @param queryCriteria
	 * @return
	 */
	public static Order convertToOrder(QueryCriteria queryCriteria)
	{
		QueryOperator operator = queryCriteria.getOperator();
		Order orden = null;
		switch (operator)
		{
		case ORDENAR_POR_ASCENDENTEMENTE:
			orden = Order.asc(queryCriteria.getAttribute());
			break;
		case ORDENAR_POR_DESCENDENTEMENTE:
			orden = Order.asc(queryCriteria.getAttribute());
			break;
		default:
			break;
		}
		return orden;
	}
}
