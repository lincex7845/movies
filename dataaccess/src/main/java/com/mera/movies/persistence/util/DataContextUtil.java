/**
 * 
 */
package com.mera.movies.persistence.util;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.jboss.logging.Logger;

/**
 * @author DavidCamilo
 * 
 * <p>This class wraps an instance of {@link SessionFactory}
 * in order to manage session life cycle</p>
 * 
 */
public class DataContextUtil {

	/**
	 * The logger instance
	 */
	private static final Logger LOGGER = Logger
			.getLogger(DataContextUtil.class);

	/**
	 * This object will be responsible for creating sessions
	 */
	private static final SessionFactory SESSIONFACTORY = buildSessionFactory();

	/**
	 * Constructor by default
	 */
	private DataContextUtil() {

	}
	
	/**
	 * Returns the current SessionFactory
	 * 
	 * @return Current SessionFactory
	 */
	public static SessionFactory getSessionFactory() {
		return SESSIONFACTORY;
	}

	/**
	 * This method returns the instance of {@link SessionFactory}
	 * according to the given configuration file
	 */
	private static SessionFactory buildSessionFactory() {

		SessionFactory sessionFactory = null;
		try {
			sessionFactory = new Configuration()
					.configure("/hibernate.cfg.xml").buildSessionFactory();
		} catch (Exception e) {
			LOGGER.error("Failed to create session factory", e);
		}
		return sessionFactory;
	}
}
