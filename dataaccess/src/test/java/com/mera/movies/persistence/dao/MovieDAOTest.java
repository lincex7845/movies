package com.mera.movies.persistence.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.After;
import org.junit.BeforeClass;
import org.junit.Test;

import com.mera.movies.persistence.entities.Image;
import com.mera.movies.persistence.entities.Movie;

public class MovieDAOTest {

	private static MovieDAO movieRepository;

	@BeforeClass
	public static void doBefore() {
		movieRepository = new MovieDAO();
	}

	@Test
	public void testAdd() {
		movieRepository.add(getDummyMovie());
		assertTrue(!movieRepository.getAll().isEmpty());
	}
	
	@After
	public void doAfter(){
		cleanData();
	}

	@Test
	public void testUpdate() {
		String newMovieCode = "123_A123";		
		movieRepository.add(getDummyMovie());
		Movie movieToUpdate = movieRepository.getAll().get(0);
		movieToUpdate.setMovieCode(newMovieCode);
		movieRepository.update(movieToUpdate);
		assertEquals(newMovieCode, movieRepository.getAll().get(0).getMovieCode());
	}

	@Test
	public void testGetAll() {
		assertNotNull(movieRepository.getAll());
	}

	@Test
	public void testGetItemById() {
//		fail("Not yet implemented");
	}

	@Test
	public void testDeleteLong() {
		movieRepository.add(getDummyMovie());
		Movie movieToDelete = movieRepository.getAll().get(0);
		movieRepository.delete(movieToDelete.getMovieId());
		assertTrue(movieRepository.getAll().isEmpty());
	}

	@Test
	public void testGetMovieWithImages() {
		movieRepository.add(getDummyMovie());
		Movie movie = movieRepository.getMovieWithImages(movieRepository.getAll().get(0));
		assertTrue(movie.getImages().isEmpty());
	}

	@Test
	public void testAddImage() {
		movieRepository.add(getDummyMovie());
		Movie movie = movieRepository.getAll().get(0);
		movieRepository.addImage(movieRepository.getMovieWithImages(movie), getDummyImage());
		movie = movieRepository.getMovieWithImages(movieRepository.getAll().get(0));
		assertTrue(!movie.getImages().isEmpty());
		
	}

	private static void cleanData() {
		List<Movie> movies = movieRepository.getAll();
		movies.forEach(movie -> movieRepository.delete(movie));
	}
	
	private Movie getDummyMovie(){
		
		Movie newMovie = new Movie();
		newMovie.setMovieCode("123-A");
		newMovie.setMovieName("A movie Test");
		newMovie.setMovieDescription("It is a test");
		return newMovie;
	}
	
	private Image getDummyImage(){
		
		Image newImage = new Image();
		newImage.setFileName("dummyImage.png");
		newImage.setBytes(new byte[10]);
		return newImage;
	}
}
