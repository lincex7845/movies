/**
 * 
 */
package com.mera.movies.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.NoSuchElementException;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;

import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mera.movies.persistence.dao.MovieDAO;
import com.mera.movies.persistence.entities.Movie;
import com.mera.movies.persistence.util.QueryCriteria;
import com.mera.movies.persistence.util.QueryOperator;

/**
 * @author DavidCamilo
 *
 */
@Path("/movies")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class MovieService {

	/**
	 * The logger instance
	 */
	private static final Logger LOGGER = Logger.getLogger(MovieService.class);

	/**
	 * Used to serialize data to json format
	 */
	private static final Gson GSON = new GsonBuilder().create();

	private static final String MOVIE_ID_PARAMETER = "IdPelicula";

	private static final String MOVIE_NAME_PARAMETER = "NombrePelicula";

	private static final String MOVIE_CODE_PARAMETER = "CodigoPelicula";

	private MovieDAO moviesRepository;

	/**
	 * Constructor by default
	 */
	public MovieService(MovieDAO moviesRepository) {
		this.moviesRepository = moviesRepository;
	}

	@GET
	public Response getMovies(@Context UriInfo uriInfo) {

		Response response;
		int status = 0;
		String entity = StringUtils.EMPTY;
		MultivaluedMap<String, String> queryParams = uriInfo
				.getQueryParameters();
		try {
			if (queryParams.size() > 0) {
				List<QueryCriteria> queryCriteria = validateParameters(queryParams);
				if (queryCriteria.isEmpty()) {
					status = Response.Status.BAD_REQUEST.getStatusCode();
					entity = "It is required sent valid parameters";
				} else {
					QueryCriteria[] arrayQueryCriteria = new QueryCriteria[queryCriteria
							.size()];
					arrayQueryCriteria = queryCriteria
							.toArray(arrayQueryCriteria);
					status = Response.Status.OK.getStatusCode();
					entity = GSON.toJson(moviesRepository
							.getMoviesByCriteria(arrayQueryCriteria));
				}
			} else {
				status = Response.Status.OK.getStatusCode();
				List<Movie> movies = moviesRepository.getAll();
				entity = GSON.toJson(movies);
			}
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();

		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	@POST
	public Response addMovie(Movie movie) {
		Response response;
		int status = 0;
		String entity = StringUtils.EMPTY;
		try {
			moviesRepository.add(movie);
			status = Response.Status.CREATED.getStatusCode();
			entity = "The movie was created";
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	@PUT
	public Response updateMovie(Movie movie) {
		Response response;
		int status = 0;
		String entity = StringUtils.EMPTY;
		try {
			moviesRepository.update(movie);
			status = Response.Status.OK.getStatusCode();
			entity = "The movie was updated";
		} catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}
	
	@DELETE
	public Response deleteMovie(@PathParam("MOVIE_ID_PARAMETER") String movieId) {
		Response response;
		int status = 0;
		String entity = StringUtils.EMPTY;
		try {
			moviesRepository.delete(Long.parseLong(movieId));
			status = Response.Status.OK.getStatusCode();
			entity = "The movie was deleted";
		}
		catch(NoSuchElementException e){
			LOGGER.error(e);
			status = Response.Status.NOT_FOUND.getStatusCode();
			entity = e.getMessage();
		}
		catch (Exception e) {
			LOGGER.error(e);
			status = Response.Status.INTERNAL_SERVER_ERROR.getStatusCode();
			entity = e.getClass().getSimpleName() + " " + e.getMessage();
		}
		response = Response.status(status).entity(entity).build();
		return response;
	}

	/**
	 * 
	 * @param queryParams
	 * @return
	 */
	private static List<QueryCriteria> validateParameters(
			MultivaluedMap<String, String> queryParams) {

		List<QueryCriteria> queryCriteria = new ArrayList<>();
		for (Entry<String, List<String>> entry : queryParams.entrySet()) {
			String key = entry.getKey();
			switch (key) {
			case MOVIE_ID_PARAMETER:
				if (StringUtils.isNotBlank(entry.getValue().get(0))) {
					queryCriteria.add(new QueryCriteria(key,
							QueryOperator.IGUAL, Long.parseLong(entry
									.getValue().get(0))));
				}
				break;
			case MOVIE_NAME_PARAMETER:
			case MOVIE_CODE_PARAMETER:
				if (StringUtils.isNotBlank(entry.getValue().get(0))) {
					queryCriteria.add(new QueryCriteria(key,
							QueryOperator.CONTIENE, entry.getValue().get(0)));
				}
				break;
			default:
				break;
			}
		}
		return queryCriteria;
	}
}
