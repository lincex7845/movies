package com.mera.movies.api;

import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import org.springframework.context.ApplicationContext;

import com.mera.movies.persistence.dao.MovieDAO;

/**
 * 
 * @author DavidCamilo
 *
 * <p>The entry point to start RESTFul Movies API</p>
 */
public class MoviesApiApplication extends Application<MoviesConfiguration> 
{

	/*
	 * (non-Javadoc)
	 * @see io.dropwizard.Application#initialize(io.dropwizard.setup.Bootstrap)
	 */
	@Override
	public void initialize(Bootstrap<MoviesConfiguration> arg0) {
		// empty
		
	}

	/*
	 * (non-Javadoc)
	 * @see io.dropwizard.Application#run(io.dropwizard.Configuration, io.dropwizard.setup.Environment)
	 */
	@Override
	public void run(MoviesConfiguration arg0, Environment arg1) throws Exception {

		final ApplicationContext context = arg0.getApplicationContext();
		final MovieDAO movieRepository = context.getBean(MovieDAO.class);
		final MovieService movieService = new MovieService(movieRepository);
		arg1.jersey().register(movieService);
	}
	
	public static void main(String[] args) throws Exception {
        new MoviesApiApplication().run(args);
    }
    
}
