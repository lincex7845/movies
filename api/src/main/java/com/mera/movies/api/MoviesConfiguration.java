/**
 * 
 */
package com.mera.movies.api;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import io.dropwizard.Configuration;

/**
 * @author DavidCamilo
 *
 */
public class MoviesConfiguration extends Configuration {

	public ApplicationContext getApplicationContext(){
		
		return new ClassPathXmlApplicationContext 
	            ("classpath:com/mera/movies/api/applicationContext.xml");		
	}
}
